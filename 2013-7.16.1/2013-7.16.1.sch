EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3700 2100 1050 450 
U 5FD5021D
F0 "reg0-12v" 50
F1 "reg0.sch" 50
$EndSheet
$Sheet
S 3700 3400 1050 450 
U 5FD507C2
F0 "reg1-5v" 50
F1 "reg1.sch" 50
$EndSheet
$Sheet
S 3700 2750 1050 450 
U 5FD50D62
F0 "reg2-adj" 50
F1 "reg2.sch" 50
$EndSheet
$Comp
L dk_Banana-and-Tip-Connectors-Jacks-Plugs:105-1102-001 J?
U 1 1 5FD65670
P 8200 5050
F 0 "J?" V 8154 5128 50  0000 L CNN
F 1 "105-1102-001" V 8245 5128 50  0000 L CNN
F 2 "digikey-footprints:Test_Jack_Horiz" H 8400 5250 60  0001 L CNN
F 3 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 5350 60  0001 L CNN
F 4 "J576-ND" H 8400 5450 60  0001 L CNN "Digi-Key_PN"
F 5 "105-1102-001" H 8400 5550 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 8400 5650 60  0001 L CNN "Category"
F 7 "Banana and Tip Connectors - Jacks, Plugs" H 8400 5750 60  0001 L CNN "Family"
F 8 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 5850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cinch-connectivity-solutions-johnson/105-1102-001/J576-ND/241121" H 8400 5950 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN TIP JACK SOLDER RED" H 8400 6050 60  0001 L CNN "Description"
F 11 "Cinch Connectivity Solutions Johnson" H 8400 6150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8400 6250 60  0001 L CNN "Status"
	1    8200 5050
	0    1    1    0   
$EndComp
$Comp
L dk_Banana-and-Tip-Connectors-Jacks-Plugs:105-1102-001 J?
U 1 1 5FD65CDC
P 8200 5250
F 0 "J?" V 8154 5328 50  0000 L CNN
F 1 "105-1102-001" V 8245 5328 50  0000 L CNN
F 2 "digikey-footprints:Test_Jack_Horiz" H 8400 5450 60  0001 L CNN
F 3 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 5550 60  0001 L CNN
F 4 "J576-ND" H 8400 5650 60  0001 L CNN "Digi-Key_PN"
F 5 "105-1102-001" H 8400 5750 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 8400 5850 60  0001 L CNN "Category"
F 7 "Banana and Tip Connectors - Jacks, Plugs" H 8400 5950 60  0001 L CNN "Family"
F 8 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 6050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cinch-connectivity-solutions-johnson/105-1102-001/J576-ND/241121" H 8400 6150 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN TIP JACK SOLDER RED" H 8400 6250 60  0001 L CNN "Description"
F 11 "Cinch Connectivity Solutions Johnson" H 8400 6350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8400 6450 60  0001 L CNN "Status"
	1    8200 5250
	0    1    1    0   
$EndComp
$Comp
L dk_Banana-and-Tip-Connectors-Jacks-Plugs:105-1102-001 J?
U 1 1 5FD66359
P 8200 5450
F 0 "J?" V 8154 5528 50  0000 L CNN
F 1 "105-1102-001" V 8245 5528 50  0000 L CNN
F 2 "digikey-footprints:Test_Jack_Horiz" H 8400 5650 60  0001 L CNN
F 3 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 5750 60  0001 L CNN
F 4 "J576-ND" H 8400 5850 60  0001 L CNN "Digi-Key_PN"
F 5 "105-1102-001" H 8400 5950 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 8400 6050 60  0001 L CNN "Category"
F 7 "Banana and Tip Connectors - Jacks, Plugs" H 8400 6150 60  0001 L CNN "Family"
F 8 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 6250 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cinch-connectivity-solutions-johnson/105-1102-001/J576-ND/241121" H 8400 6350 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN TIP JACK SOLDER RED" H 8400 6450 60  0001 L CNN "Description"
F 11 "Cinch Connectivity Solutions Johnson" H 8400 6550 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8400 6650 60  0001 L CNN "Status"
	1    8200 5450
	0    1    1    0   
$EndComp
$Comp
L dk_Banana-and-Tip-Connectors-Jacks-Plugs:105-1102-001 J?
U 1 1 5FD6674C
P 8200 5650
F 0 "J?" V 8154 5728 50  0000 L CNN
F 1 "105-1102-001" V 8245 5728 50  0000 L CNN
F 2 "digikey-footprints:Test_Jack_Horiz" H 8400 5850 60  0001 L CNN
F 3 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 5950 60  0001 L CNN
F 4 "J576-ND" H 8400 6050 60  0001 L CNN "Digi-Key_PN"
F 5 "105-1102-001" H 8400 6150 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 8400 6250 60  0001 L CNN "Category"
F 7 "Banana and Tip Connectors - Jacks, Plugs" H 8400 6350 60  0001 L CNN "Family"
F 8 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 6450 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cinch-connectivity-solutions-johnson/105-1102-001/J576-ND/241121" H 8400 6550 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN TIP JACK SOLDER RED" H 8400 6650 60  0001 L CNN "Description"
F 11 "Cinch Connectivity Solutions Johnson" H 8400 6750 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8400 6850 60  0001 L CNN "Status"
	1    8200 5650
	0    1    1    0   
$EndComp
$Comp
L dk_Banana-and-Tip-Connectors-Jacks-Plugs:105-1103-001 J?
U 1 1 5FD67AEB
P 8200 5850
F 0 "J?" V 8154 5928 50  0000 L CNN
F 1 "105-1103-001" V 8245 5928 50  0000 L CNN
F 2 "digikey-footprints:Test_Jack_Horiz" H 8400 6050 60  0001 L CNN
F 3 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 6150 60  0001 L CNN
F 4 "J577-ND" H 8400 6250 60  0001 L CNN "Digi-Key_PN"
F 5 "105-1103-001" H 8400 6350 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 8400 6450 60  0001 L CNN "Category"
F 7 "Banana and Tip Connectors - Jacks, Plugs" H 8400 6550 60  0001 L CNN "Family"
F 8 "https://belfuse.com/resources/Johnson/drawings/dr-1051101001.pdf" H 8400 6650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/cinch-connectivity-solutions-johnson/105-1103-001/J577-ND/241122" H 8400 6750 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN TIP JACK SOLDER BLACK" H 8400 6850 60  0001 L CNN "Description"
F 11 "Cinch Connectivity Solutions Johnson" H 8400 6950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8400 7050 60  0001 L CNN "Status"
	1    8200 5850
	0    1    1    0   
$EndComp
Text GLabel 8050 5050 0    50   Input ~ 0
V0_Vo
Text GLabel 8050 5450 0    50   Input ~ 0
V2_Vo
Text GLabel 8050 5250 0    50   Input ~ 0
V1_Vo
Text GLabel 8050 5650 0    50   Input ~ 0
V3_Vo
$Comp
L power:GND #PWR?
U 1 1 5FD6945F
P 8000 5900
F 0 "#PWR?" H 8000 5650 50  0001 C CNN
F 1 "GND" H 8005 5727 50  0000 C CNN
F 2 "" H 8000 5900 50  0001 C CNN
F 3 "" H 8000 5900 50  0001 C CNN
	1    8000 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 5900 8000 5850
Wire Wire Line
	8000 5850 8050 5850
$Sheet
S 3700 4050 1050 450 
U 5FD51305
F0 "reg3-neg" 50
F1 "reg3.sch" 50
$EndSheet
$Sheet
S 2700 2100 900  2400
U 5F891ECE
F0 "power" 50
F1 "psu.sch" 50
$EndSheet
$Sheet
S 4850 2100 900  2400
U 5F899CCC
F0 "panel" 50
F1 "panel.sch" 50
$EndSheet
$EndSCHEMATC
